<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');

            $table->bigInteger('bill_id')->unsigned()->nullable();
            $table->foreign('bill_id')
                ->references('id')->on('bills');

            $table->bigInteger('payment_methods_id')->unsigned()->nullable();
            $table->foreign('payment_methods_id')
                ->references('id')->on('payment_methods');
                

            $table->string('description');
            $table->bigInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_method');
    }
}
