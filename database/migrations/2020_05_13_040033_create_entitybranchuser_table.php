<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitybranchuserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_branch_user', function (Blueprint $table) {
            $table->bigIncrements('id');
                $table->bigInteger('entity_branch_id')->unsigned()->nullable();
                $table->foreign('entity_branch_id')
                    ->references('id')->on('entity_branch');
    
                $table->bigInteger('user_id')->unsigned()->nullable();
                $table->foreign('user_id')
                    ->references('id')->on('users');
                    
                $table->bigInteger('status');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_branch_user');
    }
}
