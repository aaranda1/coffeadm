<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Clave principal autoincremental');
            $table->string('code', 10)->comment('Código');
            $table->bigInteger('commune_id')->unsigned()->comment('Identificador de la ciudad');
            $table->foreign('commune_id')
                ->references('id')->on('communes');
            $table->string('name')->comment('Nombre de la localidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localities');
    }
}
