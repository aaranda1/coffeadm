<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cuantity');
            $table->string('description');
            $table->bigInteger('bill_id')->unsigned()->nullable();
            $table->foreign('bill_id')
                ->references('id')->on('bills');
            $table->bigInteger('product_branch_id')->unsigned()->nullable();
            $table->foreign('product_branch_id')
                    ->references('id')->on('product_branch');
            $table->bigInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_details');
    }
}
