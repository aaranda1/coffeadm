<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitybranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_branch', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('entity_id')->unsigned()->nullable();
            $table->foreign('entity_id')
                ->references('id')->on('entities');

            $table->bigInteger('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')
                ->references('id')->on('branches');
            $table->bigInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_branch');
    }
}
