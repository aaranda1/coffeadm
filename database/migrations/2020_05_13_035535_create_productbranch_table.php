<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductbranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_branch', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('discount');
                $table->bigInteger('product_id')->unsigned()->nullable();
                $table->foreign('product_id')
                    ->references('id')->on('products');
    
                $table->bigInteger('branch_id')->unsigned()->nullable();
                $table->foreign('branch_id')
                    ->references('id')->on('branches');
                    
                $table->bigInteger('status');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_branch');
    }
}
