<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserRole;
use App\Entity;
use App\Branch;
use App\EntityBranch;
use App\EntityBranchUser;
class TestAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        
        $user->forenames = "Juanito" ;
        $user->surnames = "owner";
        $user->email          ="owner@cl.cl";
        $user->phone          ="+56988776655";   
        $user->rut = "12312312-3" ;   
        $user->password  = bcrypt("123123");
        $user->status     = 1;
        $user->save();
        $userRole = new UserRole;
        $userRole->user_id = $user->id;
        $userRole->role_id = 2;
        $userRole->status = UserRole::ACTIVE;
        $userRole->save();
        $entity1 = new Entity;
        $entity1->name = "Cafe Literario";
        $entity1->rut = "12312312-3" ;
        $entity1->status = 1;
        $entity1->contact_phone = "11111" ;
        $entity1->address = "calle #123" ;
        $entity1->city_id =9;
        $entity1->save();
        $conection = new EntityBranch;
        $conection->entity_id = $entity1->id;
        $conection->status = EntityBranch::ACTIVE;
        $conection->save();
        $EBU = new EntityBranchUser;
        $EBU->entity_branch_id = $conection->id;
        $EBU->user_id = $user->id;
        $EBU->status = EntityBranchUser::ACTIVE;
        $EBU->save();
        
            $branch1 = new Branch;
            $branch1->address = "Viena" ;
            $branch1->name = $entity1->name." ".$branch1->address;
            $branch1->entity_rut = $entity1->rut ;
            $branch1->status = 1;
            $branch1->contact_phone = "11111" ;
            $branch1->city_id =1;
            $branch1->save();        
            
            $conection = new EntityBranch;
            $conection->entity_id = $entity1->id;
            $conection->branch_id = $branch1->id;
            $conection->status = EntityBranch::ACTIVE;
            $conection->save();


                $worker = new User;
                $worker->forenames = "Pepe" ;

                $worker->surnames = "Admin";
                $worker->email   ="admin01@cl1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 3;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Alex" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter01@cl1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Cata" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter02@cl1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Diego" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter03@cl1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();




                $worker = new User;
                $worker->forenames = "Edo" ;
                $worker->surnames = "Cashier";
                $worker->email   ="cashier01@cl1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 5;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


            $branch1 = new Branch;
            $branch1->address = "Colon" ;
            $branch1->name = $entity1->name." ".$branch1->address;
            $branch1->entity_rut = $entity1->rut ;
            $branch1->status = 1;
            $branch1->contact_phone = "11111" ;
            $branch1->city_id =5;
            $branch1->save();        
            
            $conection = new EntityBranch;
            $conection->entity_id = $entity1->id;
            $conection->branch_id = $branch1->id;
            $conection->status = EntityBranch::ACTIVE;
            $conection->save();


                $worker = new User;
                $worker->forenames = "Felipe" ;
                $worker->surnames = "Admin";
                $worker->email   ="admin01@cl2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 3;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Andy" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter01@cl2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Pablo" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter02@cl2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Cony" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter03@cl2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();
                


                $worker = new User;
                $worker->forenames = "Camila" ;
                $worker->surnames = "Cashier";
                $worker->email   ="cashier01@cl2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 5;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();




        $user = new User;

        $user->forenames = "Mariana" ;
        $user->surnames = "owner";
        $user->email          ="owner@cv.cl";
        $user->phone          ="+56988776655";   
        $user->rut = "12312312-3" ;   
        $user->password  = bcrypt("123123");
        $user->status     = 1;
        $user->save();

        $userRole = new UserRole;
        $userRole->user_id = $user->id;
        $userRole->role_id = 2;
        $userRole->status = UserRole::ACTIVE;
        $userRole->save();
        $entity1 = new Entity;
        $entity1->name = "Cafe Valpo";
        $entity1->rut = "12312312-3" ;
        $entity1->status = 1;
        $entity1->contact_phone = "11111" ;
        $entity1->address = "calle #123" ;
        $entity1->city_id =1;
        $entity1->save();
        $conection = new EntityBranch;
        $conection->entity_id = $entity1->id;
        $conection->status = EntityBranch::ACTIVE;
        $conection->save();
        $EBU = new EntityBranchUser;
        $EBU->entity_branch_id = $conection->id;
        $EBU->user_id = $user->id;
        $EBU->status = EntityBranchUser::ACTIVE;
        $EBU->save();

            $branch1 = new Branch;
            $branch1->address = "Victoria" ;
            $branch1->name = $entity1->name." ".$branch1->address;
            $branch1->entity_rut = $entity1->rut ;
            $branch1->status = 1;
            $branch1->contact_phone = "11111" ;
            $branch1->city_id =7;
            $branch1->save();        
            
            $conection = new EntityBranch;
            $conection->entity_id = $entity1->id;
            $conection->branch_id = $branch1->id;
            $conection->status = EntityBranch::ACTIVE;
            $conection->save();


                $worker = new User;
                $worker->forenames = "Chalie" ;

                $worker->surnames = "Admin";
                $worker->email   ="admin01@cv1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 3;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Jhon" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter01@cv1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Alvaro" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter02@cv1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Patricio" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter03@cv1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();




                $worker = new User;
                $worker->forenames = "Marcela" ;
                $worker->surnames = "Cashier";
                $worker->email   ="cashier01@cv1.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 5;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


            $branch1 = new Branch;
            $branch1->address = "España" ;
            $branch1->name = $entity1->name." ".$branch1->address;
            $branch1->entity_rut = $entity1->rut ;
            $branch1->status = 1;
            $branch1->contact_phone = "11111" ;
            $branch1->city_id =2;
            $branch1->save();        
            
            $conection = new EntityBranch;
            $conection->entity_id = $entity1->id;
            $conection->branch_id = $branch1->id;
            $conection->status = EntityBranch::ACTIVE;
            $conection->save();


                $worker = new User;
                $worker->forenames = "Phil" ;
                $worker->surnames = "Admin";
                $worker->email   ="admin01@cv2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 3;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Maria" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter01@cv2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                    $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Javiera" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter02@cv2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();


                $worker = new User;
                $worker->forenames = "Fran" ;
                $worker->surnames = "Waiter";
                $worker->email   ="waiter03@cv2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 4;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();
                


                $worker = new User;
                $worker->forenames = "Gabriela" ;
                $worker->surnames = "Cashier";
                $worker->email   ="cashier01@cv2.cl";
                $worker->phone   ="+56988776655";      
                $worker->password  = bcrypt("123123");
                $worker->status     = 1;
                $worker->rut = "11111111-1" ;
                $worker->save();
                $userRole = new UserRole;
                $userRole->user_id = $worker->id;
                $userRole->role_id = 5;
                $userRole->status = UserRole::ACTIVE;
                $userRole->save();
                $EBU = new EntityBranchUser;
                $EBU->entity_branch_id = $conection->id;
                $EBU->user_id = $worker->id;
                $EBU->status = EntityBranchUser::ACTIVE;
                $EBU->save();
    }
}
