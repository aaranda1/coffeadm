<?php


use Illuminate\Database\Seeder;
use App\Region;
use App\Commune;
use App\City;

class SoftLocalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$regions = [
            [
                'name' => 'Region 1',
                'cities' => [
                    [ 
                        'name' => 'ciudad 1',
                        
                    ]
                ]
            ],
            [
                'name' => 'Region 2'
            ]
        ];*/
        $regions = json_decode(File::get(database_path() . '/seeds/unique_territorial_codes_region_chile_2018.json'));
        foreach ($regions as $region) {
            $new_region = new Region;
            $new_region->code= $region->cut;
            $new_region->name= $region->name;
            $new_region->save();
          
               
        }
        $provinces = json_decode(File::get(database_path() . '/seeds/unique_territorial_code_provinces_chile_2018.json'));
        foreach ($provinces as $province){
            $region = Region::where('code', $province->region_cut)->first();
            $row = new Commune;
            $row->region_id = $region->id;
            $row->code= $province->province_cut;
            $row->name= $province->province_name;
            $row->save();
        }

        $localities = json_decode(File::get(database_path() . '/seeds/unique_territorial_codes_communes_chile_2018.json'));
        foreach ($localities as $City){
            $province = Commune::where('code', $City->province_cut)->first();
            $row = new City;
            $row->commune_id = $province->id;
            $row->code= $City->commune_cut;
            $row->name= $City->commune_name;
            $row->save();
        }
        
    }
}
