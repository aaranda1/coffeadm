<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;
        $role->name = "Superadmin";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();

        $role = new Role;
        $role->name = "Owner";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();

        $role = new Role;
        $role->name = "Administrator";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();

        $role = new Role;
        $role->name = "Waiter";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();

        $role = new Role;
        $role->name = "Cashier";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();

        $role = new Role;
        $role->name = "Chef";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();

        $role = new Role;
        $role->name = "Busboy";
        $role->description = "";
        $role->status = Role::ACTIVE;
        $role->save();
    }
}
