<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserRole;
class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        
        $user->forenames = "SuperAdmin" ;
        $user->surnames = "CoffeADM";
        $user->email          ="superadmin@coffeadm.cl";
        $user->phone          ="+56988776655";      
        $user->password  = bcrypt("123123");
        $user->status     = 1;
        $user->save();

        $userRole = new UserRole;
        $userRole->user_id = $user->id;
        $userRole->role_id = 1;
        $userRole->status = UserRole::ACTIVE;
        $userRole->save();
    }
}
