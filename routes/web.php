<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();


Route::get('/register', function () {
    return view('auth.register'
);
});
Route::get('/regions', 'LocationController@getRegions');
Route::get('/communes/{region_id}', 'LocationController@getCommunes');
Route::get('/cities/{commune_id}', 'LocationController@getCities');

Route::resource('entitybranchuser', 'EntityBranchUserController');

Route::post('entities', 'EntityController@store');
Route::get('/checkEntityRut/{rut}', 'EntityController@checkEntityRut');


Route::get('/checkRut/{rut}', 'UserController@checkRut');
Route::get('/checkEmail/{email}', 'UserController@checkEmail');

Route::get('/sendConfirmationMail/{id}', 'UserController@sendConfirmationMail');

Route::get('/accountConfirmation/{hash}', 'UserController@confirmationAccount');

Route::resource('users', 'UserController');
Route::resource('entities', 'EntityController');

Route::group(['middleware' => ['auth']], function(){

    Route::get('users', 'UserController@index');
    Route::get('/home', function () {
        return view('welcome');
    });
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('users/{entity_filter}/{branch_filter}/{bar_filter}', 'UserController@userFilter');
    Route::get('entities', 'EntityController@index');
  
});