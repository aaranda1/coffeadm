<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class invitationCreateCompany extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $account;
    public $company;

    public function __construct($user, $entity)
    {
        $this->user     =   $user;
        $this->company  =   $entity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                    ->from('info@CoffeADM.cl','CoffeADM')
                    ->view('emails.invitationCreateCompany')
                    ->subject('Confirmación de registro CoffeADM') 
                     ->with([
                 'company_name' => $this->company->name,        
                 'name'     =>  $this->user->forenames." ".$this->user->surnames,
                 'token'    =>  $this->user->register_token

                     ]);
    }
}
