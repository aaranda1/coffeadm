<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Entity extends Model
{
    public $table= "entities";
    
    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;
    CONST AWAITING = 3;

    protected $fillable = [
        'name','rut','address','contact_phone','status',
    ];

    public function branches()
    {
        return $this->hasMany('App\EntityBranch')
        ->join("branches","entity_branch.branch_id","branches.id")
        ;
    }
}
