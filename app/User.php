<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    public $table= "users";

    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;
    CONST AWAITING = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'forenames','surnames','phone','status', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function roles(){
        return $this
        ->hasMany('App\UserRole')
        ->join("roles","user_roles.role_id","roles.id")
        
           /*  ->where('account_role.status', AccountRole::ACTIVE) */;
      }
    public function entity(){

        return $this->hasMany('App\EntityBranchUser') 
        ->join("entity_branch","entity_branch_user.entity_branch_id","entity_branch.id")
        ->join("entities","entity_branch.entity_id","entities.id")
        ->first();

/*         return $this->belongsTo('App\Entity')
        ->join("entity_branch","entity_branch.entity_id","entities.id")
        ->join("entity_branch_user","entity_branch.entity_id","entities.id"); */

           /*  ->where('account_role.status', AccountRole::ACTIVE) */;
      }
      public function branch(){

        return $this->hasMany('App\EntityBranchUser') 
        ->join("entity_branch","entity_branch_user.entity_branch_id","entity_branch.id")
        ->join("branches","entity_branch.branch_id","branches.id")
        ->first();

/*         return $this->belongsTo('App\Entity')
        ->join("entity_branch","entity_branch.entity_id","entities.id")
        ->join("entity_branch_user","entity_branch.entity_id","entities.id"); */

           /*  ->where('account_role.status', AccountRole::ACTIVE) */;
      }
}
