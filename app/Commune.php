<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
}
