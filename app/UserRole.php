<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class UserRole extends Model
{
    public $table= "user_roles";

    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;
}
