<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function commune()
    {
        return $this->belongsTo('App\Commune');
    }
}
