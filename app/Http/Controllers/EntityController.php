<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity;
use App\EntityBranch;

class EntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $entities = Entity::join("cities","cities.id","entities.city_id")
                            ->join("communes","communes.id","cities.commune_id")
                            ->join("regions","regions.id","communes.region_id")
                            ->select("entities.*",
                                    "cities.name AS city_name",
                                    "communes.name AS commune_name",
                                    "regions.name AS region_name")
                            ->get();
                           // dd($entities);
       return view('modules.entities.index', compact('entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $new_entity = new Entity;
        $new_entity->rut = strtolower($request->input("rut"));
        $new_entity->name = $request->input("name");
        $new_entity->address = $request->input("address");
        $new_entity->city_id = $request->input("city");
        $new_entity->contact_phone = $request->input("contact_phone");
        $new_entity->status = Entity::AWAITING ;
        $new_entity->save(); 

        $conection = new EntityBranch;
        $conection->entity_id = $new_entity->id;
        $conection->status = EntityBranch::ACTIVE;
        $conection->save();

        return $this->returnApiSuccess([$conection], 'entity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function checkEntityRut($rut)
    {
        $entity = Entity::where("rut",$rut)->first();

        return $this->returnApiSuccess([$entity], 'entity');
    }
}
