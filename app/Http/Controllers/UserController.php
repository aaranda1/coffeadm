<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Entity;
use App\Branch;
use Carbon\Carbon;
use App\UserRole;
use Illuminate\Support\Facades\Mail;
use App\Mail\invitationCreateCompany;
use Illuminate\Support\Facades\Auth;
use DB;
use Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $currentUser = Auth::user();
        $entities = Entity::with("branches");
        $branches  = null;
        $users = User::join("user_roles","users.id","user_roles.user_id")
            ->join("roles","roles.id","user_roles.role_id")
            ->join("entity_branch_user","entity_branch_user.user_id","users.id")
            ->join("entity_branch","entity_branch_user.entity_branch_id","entity_branch.id")
            ->select("users.*",
                    "roles.id AS role_id"
            )
            ->where("users.status",User::ACTIVE);

        if($currentUser->hasAnyRole(['Superadmin'])){
           $roles = [2,3,4,5,6,7];
           $role_list = [
                            ['id' => 2, 'name' => 'Dueño'],
                            ['id' => 3, 'name' => 'Administrador'],
                            ['id' => 4, 'name' => 'Mesero'],
                            ['id' => 5, 'name' => 'Cajero'],
                            ['id' => 6, 'name' => 'Cocinero'],
                            ['id' => 7, 'name' => 'Ayudante']  
                        ];
        }
        else{

            $entities->where("id",Auth::user()->entity()->entity_id);
            $branches  = Branch::join("entity_branch","entity_branch.branch_id","branches.id")
                                ->where("entity_branch.entity_id",Auth::user()->entity()->entity_id)
                                ->select("branches.*");
            $users->where("entity_branch.entity_id",Auth::user()->entity()->entity_id);
            if($currentUser->hasAnyRole(['Owner'])){
                $roles = [3,4,5,6,7];
                $role_list = [
                    ['id' => 3, 'name' => 'Administrador'],
                    ['id' => 4, 'name' => 'Mesero'],
                    ['id' => 5, 'name' => 'Cajero'],
                    ['id' => 6, 'name' => 'Cocinero'],
                    ['id' => 7, 'name' => 'Ayudante']  
                ];
            }
            elseif($currentUser->hasAnyRole(['Administrator'])){
                $branches->where("entity_branch.branch_id",Auth::user()->branch()->branch_id);
                $roles = [4,5,6,7];
                $role_list = [
                    ['id' => 4, 'name' => 'Mesero'],
                    ['id' => 5, 'name' => 'Cajero'],
                    ['id' => 6, 'name' => 'Cocinero'],
                    ['id' => 7, 'name' => 'Ayudante']  
                ];
                $users->where("entity_branch.branch_id",Auth::user()->branch()->branch_id);
    
            }

        }

        $users = $users->whereIn("roles.id",$roles);

       return view('modules.users.index', compact('entities','users','branches','role_list'));
       
    }
    public function userFilter($entity_filter, $branch_filter, $bar_filter)
    {

        $currentUser = Auth::user();

        $users = User::join("user_roles","users.id","user_roles.user_id")
            ->join("roles","roles.id","user_roles.role_id")
            ->join("entity_branch_user","entity_branch_user.user_id","users.id")
            ->join("entity_branch","entity_branch_user.entity_branch_id","entity_branch.id")
            ->select("users.*",
                    "roles.id AS role_id"
            )
            ->where("users.status",User::ACTIVE);

        if($currentUser->hasAnyRole(['Superadmin'])){

           $roles = [2,3,4,5];

        }
        else{

            $users->where("entity_branch.entity_id",$currentUser->entity()->entity_id);

            if($currentUser->hasAnyRole(['Owner'])){
                $roles = [3,4,5];

            }
            elseif($currentUser->hasAnyRole(['Administrator'])){
                $roles = [4,5];
                $users->where("entity_branch.branch_id",$currentUser->branch()->branch_id);
            }

        }

        $users = $users->whereIn("roles.id",$roles);

        if($entity_filter != 0){

            $users->where("entity_branch.entity_id", $entity_filter);
 
        }
        if($branch_filter != 0){

            $users->where("entity_branch.branch_id",$branch_filter);

        }
        if($bar_filter != "---"){

            $users->where(function($query) use($bar_filter) {
                $query->where(\DB::raw('LOWER(users.forenames)'), 'like', '%' . strtolower($bar_filter) . '%')
                    ->orWhere(\DB::raw('LOWER(users.surnames)'), 'like', '%' . strtolower($bar_filter) . '%')
                    ->orWhere(\DB::raw('LOWER(users.email)'), 'like', '%' . strtolower($bar_filter) . '%')
                    ->orWhere(\DB::raw('LOWER(users.rut)'), 'like', '%' . strtolower($bar_filter) . '%');
            });
        }


        return $this->returnApiSuccess($users->get(), 'users');
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkRut($rut)
    {
        $user = User::where("rut",$rut)->first();

        return $this->returnApiSuccess([$user], 'user');
        
    }
    public function checkEmail($email)
    {
        $user = User::where("email",$email)->first();

        return $this->returnApiSuccess([$user], 'user');
    }
    public function confirmationAccount($hash)
    {
        $user = User::where("register_token",$hash)
                    ->where("status",User::AWAITING)            
                    ->first();

        if($user){

            $user->status = User::ACTIVE; 
            $user->save();
            
            return view('modules.confirmated');
        }
    }
    public function store(Request $request)
    {
       
        $data = $request->all();

        $new_user = new User;
        $new_user->rut = strtolower($request->input("rut"));
        $new_user->forenames = $request->input("forenames");
        $new_user->surnames = $request->input("surnames");
        $new_user->email = strtolower($request->input("email"));
        $new_user->phone = $request->input("phone");
        $new_user->status = User::AWAITING ;
        $new_user->password = bcrypt($request->input("password"));
        $new_user->register_token = md5(Carbon::now());
        $new_user->save(); 
        
        $role = Role::where("name","Owner")->first();

        $user_role = new UserRole;
        $user_role->user_id = $new_user->id;
        $user_role->role_id = $role->id;
        $user_role->status = UserRole::ACTIVE;
        $user_role->save();

        return $this->returnApiSuccess([$new_user], 'user');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function sendConfirmationMail($id)
    {
        $user = User::where("id",$id)
                    ->first();
        $entity = Entity::join("entity_branch","entity_branch.entity_id","entities.id")
                            ->join("entity_branch_user","entity_branch_user.entity_branch_id","entity_branch.id")
                            ->join("users","users.id","entity_branch_user.user_id")
                            ->where("users.id",$id)
                            ->select("entities.*")
                            ->first();
                          //  dd($entity);

        \Mail::to($user->email)->send(new invitationCreateCompany($user, $entity));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
