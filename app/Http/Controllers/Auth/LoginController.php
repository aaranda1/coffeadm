<?php

namespace App\Http\Controllers\Auth;

use App\Account;
use App\Rules\cuentaConfirmada;
use App\Rules\cuentaActiva;
use App\Rules\cuentaAceptada;
use App\Rules\cuentaExiste;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/secondment';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
{

    $this->guard()->logout();

    $request->session()->invalidate();

    return redirect('/');
}

    public function username()
    {
        return 'email';
    }

    protected function authenticated(Request $request)
{


}  

    protected function validateLogin(Request $request)
    {
      
        
        $this->validate($request, [

            $this->username() => ['required','required',new cuentaExiste,new cuentaConfirmada],
            'password' => 'required',
        ]);
        
    }

}