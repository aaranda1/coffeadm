<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EntityBranchUser extends Model
{
    public $table= "entity_branch_user";
    
    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;

}
