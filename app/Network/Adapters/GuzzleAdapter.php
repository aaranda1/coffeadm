<?php
namespace App\Network\Adapters;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use App\Network\RestResponse;

class GuzzleAdapter implements RestInterface
{
    protected $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    public function get($url, $params =  [], $headers =  [])
    {
        $request_result =  $this->client->request('GET', $url, ['headers' => $headers, 'form_params' => $params, 'http_errors' => false]);
        return $this->execute($request_result);
    }

    public function post($url, $params =  [], $headers =  [])
    {
        $request_result =  $this->client->request('POST', $url, ['headers' => $headers, 'body' => $params, 'http_errors' => false]);
        return $this->execute($request_result);
    }

    public function put($url, $params =  [], $headers =  [])
    {
        $request_result =  $this->client->request('PUT', $url, ['headers' => $headers, 'form_params' => $params, 'http_errors' => false]);
        return $this->execute($request_result);
    }

    public function patch($url, $params =  [], $headers =  [])
    {
        $request_result =  $this->client->request('PATCH', $url, ['headers' => $headers, 'body' => $params, 'http_errors' => false]);
        return $this->execute($request_result);
    }

    public function delete($url, $params =  [], $headers =  [])
    {
        $request_result =  $this->client->request('delete', $url, ['headers' => $headers, 'form_params' => $params, 'http_errors' => false]);
        return $this->execute($request_result);
    }

    public function execute($response)
    {
        $body = $response->getBody()->getContents();
        $status = $response->getStatusCode();

        return ['body' => $body, 'status' => $status];
    }
}
