<?php
namespace App\Network\Adapters;

interface RestInterface
{
    public function get($url, $params, $headers);
    public function post($url, $params, $headers);
    public function put($url, $params, $headers);
    public function patch($url, $params, $headers);
    public function delete($url, $params, $headers);
    public function execute($response);

}
