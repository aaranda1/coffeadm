<?php
namespace App\Network;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use App\Network\Adapters\GuzzleAdapter;

class RestGuzzle
{
    private $client;

    public function __construct(GuzzleAdapter $restAdapter)
    {
        $this->client = new Client();
    }

    public function get($params,$headers, $url)
    {
        return $this->client->get($params,$headers, $url);
    }
    public function post($params,$headers, $url)
    {
        return $this->client->post($params,$headers, $url);
    }
    public function put($params,$headers, $url)
    {
        return $this->client->put($params,$headers, $url);
    }
    public function patch($params,$headers, $url)
    {
        return $this->client->patch($params,$headers, $url);
    }
    public function delete($params,$headers, $url)
    {
        return $this->client->delete($params,$headers, $url);
    }

    public function getStatusCode()
    {
        return $this->client->getStatusCode();
    }
}
