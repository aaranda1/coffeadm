<?php
namespace App\Network;
use App\Network\Adapters\GuzzleAdapter;
use App\Network\Adapters\RestInterface;
class Rest
{
    private $restAdapter;

    public function __construct(RestInterface $restAdapter)
    {
        $this->restAdapter = $restAdapter;
    }

    public function get($url, $params,$headers)
    {
        return $this->restAdapter->get($url, $params, $headers);
    }
    public function post($url, $params,$headers)
    {
        return $this->restAdapter->post($url, $params,$headers);
    }
    public function put($url, $params,$headers)
    {
        return $this->restAdapter->put($url, $params,$headers);
    }
    public function patch($url, $params,$headers)
    {
        return $this->restAdapter->patch($url, $params,$headers);
    }
    public function delete($url, $params,$headers)
    {
        return $this->restAdapter->delete($url, $params,$headers);
    }

    public function getStatusCode()
    {
        return $this->restAdapter->getStatusCode();
    }

    public function getBody()
    {
        return $this->restAdapter->getBody();
    }
}
