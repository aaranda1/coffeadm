<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Role extends Model
{
    public $table= "roles";

    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;

}
