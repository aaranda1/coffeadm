<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Branch extends Model
{
    public $table= "branches";
    
    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;
    CONST AWAITING = 3;

    protected $fillable = [
        'name','rut','address','contact_phone','status','entity_rut'
    ];
}
