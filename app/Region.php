<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function localities()
    {
        return $this->hasMany('App\Locality');
    }
}
