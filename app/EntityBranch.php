<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class EntityBranch extends Model
{
    public $table= "entity_branch";
    
    use  Notifiable;

    CONST DELETED =0;
    CONST ACTIVE=1;
    CONST AWAITING = 3;

}
