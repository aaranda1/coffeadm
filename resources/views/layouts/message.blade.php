@if (Session::has('success'))
<div class="alert alert-success form_feedback mt-2" role="alert">
    <strong>{{session()->get('success') }}</strong>
    <button type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (Session::has('errors'))

<div class="alert alert-danger form_feedback mt-2" role="alert">
    
        @foreach ($errors->all() as $error)
        <p class="m-0 small">{{$error}}</p>
{{--         @if($error == "Esta cuenta no ha sido confirmada.")
        <a class="m-0 small"   onclick='submitResend()' style="    background: none;
        border: none;
        color: blue; text-decoration: underline;
        cursor: pointer;">Renviar Confirmación</a>

        @endif --}}
        @endforeach
</div>


@endif

@if (session('error'))
<div class="alert alert-danger">
    {{ session('error') }}
</div>
@endif
