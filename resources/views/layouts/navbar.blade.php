
<nav class="navbar navbar-expand-xl navbar-light bg-light">

   {{--  @if(Auth::user()->currentAccount()->hasAnyRole(['admin', 'superadmin'])) --}}
    <div class="container-fluid">
        <a class="navbar-brand" href="/home"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav mr-auto text-center">
                @if(Auth::user()->hasAnyRole(['Superadmin']))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Entidades</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="/entities">Habilitadas</a>
                            <a class="dropdown-item" href="/openupcompanies">En espera</a>
                        </div>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="">Sucursales<span class="sr-only"></span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trabajadores</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="/users">Trabajadores Activos</a>
                        <a class="dropdown-item" href="{{-- {{'/companies/'.Auth::user()->currentAccount()->company->id."/inactiveUsers"}} --}}">Trabajadores en espera</a>
                       {{--  <a class="dropdown-item" href="{{'/companies/'.Auth::user()->currentAccount()->company->id."/invitations"}}">Invitaciónes</a> --}}
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reporte</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="/ownerReportView">Propinas</a>
                        <a class="dropdown-item" href="/announcerReportView">Ventas</a>
                        <a class="dropdown-item" href="/adminReport">Stock</a>
                    </div>
                </li>
            </ul>
            <div class="my-2 my-md-0 dd text-center">


                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user mr-2"></i>{{Auth::user()->forenames}}</button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{-- {{'/users/'.Auth::user()->getId().'/profile/'}} --}}">Editar perfil</a>
                        <a class="dropdown-item" href="{{-- {{'/companies/'.Auth::user()->currentAccount()->company_id.'/profile/'}} --}}">Editar empresa</a>
                        <a href="{{-- {{ route('logout') }} --}}" class="dropdown-item c-danger" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            Cerrar Sesión
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>

                   
                </div>

            </div>
        </div>
    </div>

    
 </nav>
