<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>


        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>CoffeADM</title>
        <link rel="icon" href="{!! asset('images/coffeLogo.png') !!}" />
        <!-- Styles -->
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/vue-select.css')}}">
    </head>
    <body>
        <main>
            @yield('navbar')
            <div id="app">
                @yield('content')
            </div>
        </main>
        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js')}}"></script>
        <script src="{{ asset('js/bootstrap.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
       {{--  <script src="{{ asset('js/jquery.min.js')}}"></script> --}}
        <script src="{{ asset('js/app.general.js') }}"></script>
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="https://kit.fontawesome.com/6e99aa4256.js"></script>
        
        @yield('template_scripts')
    </body>
</html>