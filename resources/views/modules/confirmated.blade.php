<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Confirmación</title>
  <link
    rel="stylesheet"
    href="https://cdn.i-med.cl/medis/1.1.0/css/imed-ds.css">
  <link
    rel="stylesheet"
    href="https://pro.fontawesome.com/releases/v5.10.1/css/all.css"
    integrity="sha384-y++enYq9sdV7msNmXr08kJdkX4zEI1gMjjkw0l9ttOepH7fMdhb7CePwuRQCfwCr"
    crossorigin="anonymous">

</head>
<body class="body-gray">
<main class="main">

  <div class="container">
    <div class="row mt-5">
      <div class="col-sm-6 mx-auto">
        <div class="card shadow-sm mb-5">

          <hr class="m-0">
          <div class="card-body">
            <div class="d-flex justify-content-center">
              <div class="col-6 text-center mt-3 ">
                  <div class="btn-group-toggle mx-auto" >
                      <i class="far fa-mug-hot d-block fa-3x d-block mx-auto mb-4 " width="50%" ></i>
                      <img class="mb-4 " src="https://i.ibb.co/P6hDsN8/output-onlinepngtools.png" alt="" width="50%" />
                      
                  </div>
              </div>
          </div>
            <h6 class="text-center">Genial!</h6>
            <p class="text-center mb-5">Tu cuenta ha sido confirmada con éxito</p>
            <div class="row">
              <div class="col-sm-6 mx-auto">
                <a  href="{{URL::to('/login')}}" style="background: black;" class="btn btn-primary btn-block">Entendido</a>
               
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 mx-auto">
            <img src="http://jumpitt.com/imed/mimed-cuenta/imed-blue.svg" alt="" class="d-block img-fluid mx-auto">
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</body>
</html>
