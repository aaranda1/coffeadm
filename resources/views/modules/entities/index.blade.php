@extends('layouts.app')
@section('navbar')
@include('layouts.navbar')
@endsection

@section('content')

<div class="container">
<div class="container">
 	<div class="row">
		<div class="col-12 mt-2">
			{{-- @include("layouts.message") --}}
		</div>
	</div> 
	<div class="row">
		<div class="col-12 mt-2 small">
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 d-flex align-items-center justify-content-between">
			<h5 class="mb-0 d-inline-block">Atributos</h5>

		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card mt-3 table-responsive">
				<table class="table table-hover table-borderless nm">
					<thead>
						<tr>
							<th>{!! 'Nombre' !!}</th>
							<th>{!! 'Rut' !!}</th>
							<th>{!! 'Telefono' !!}</th>
							<th>{!! 'Direccion' !!}</th>
							<th class="no-search no-sort">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($entities as $entity)
						<tr>
							<td >{{$entity->name}}</td>
							<td >{{$entity->rut}}</td>
							
							<td >{{$entity->contact_phone}}</td>
							<td >{{$entity->address.", ".$entity->city_name.", ".$entity->region_name}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


</div>

@endsection
