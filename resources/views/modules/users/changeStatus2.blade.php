@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-10 col-md-6 mx-auto">
            <div class="card mt-5">
                <div class="card-header">
                <i class="far fa-check-circle c-primary"></i>
                    <h5 class="card-title mb-0 d-inline-block">Confirmación de Registro de empresa</h5> 
                </div> 
                    <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{route('changeAccountStatus')}}">

                                <div>
                                <i class="far fa-user c-primary"></i>
                                <label>Nombre del solicitante</label>
                                <div class="col-12">
                                    @if($user_send_invitation!=null)
                                    <strong>{{$user_send_invitation->forenames.' '.$user_send_invitation->surnames }}</strong> te ha invitado como <strong>{{$account->roles()->first()->description}} </strong>
                                    @else
                                    <strong>{{$user->forenames.' '.$user->surnames }}</strong>
                                    @endif
                                
                                </div>
                                </div>
                                <br/>

                                <div>
                                <i class="far fa-address-book c-primary"></i>
                                <label>Nombre de la Empresa</label>
                                <div class="col-12">
                                <strong>{{$account->company->name}}</strong>
                                </div>
                                </div>
                                <br/>
                                   
                                <div>
                                <i class="fas fa-map-marker-alt c-primary"></i>
                                <label>Dirección de la Empresa</label>
                                <div class="col-12">
                                 <strong>{{$account->company->address}}</strong> 
                                </div>
                                </div>
                                <br/>

                                <div class="mt-4">
                                <div class="col-12"> 
                                 {{ csrf_field() }}
                                <input type="hidden" id="account_id" name="account_id" value="{{$account->id}}" />
                                <input type="hidden" id="tokenAccount" name="tokenAccount" value="{{$account->token}}" />
                                <input type="hidden" id="SelectedOption" name="SelectedOption" value="1" />
                                <button type="submit" class="btn btn-outline-primary">
                                Confirmar
                                </button>
                                </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection