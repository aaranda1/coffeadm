@extends('layouts.app')


@section('template_title')

@endsection

@section('template_linked_css')

@endsection
@section('content')
<register-view inline-template>
<div v-cloak>


<section>
    <form id="signin_form" method="POST" >
        {{ csrf_field() }}
        <div class="container">
            <div id="alert_msg" class="mt-3">
                @include("layouts.message")
            </div>
        </div>
        <div class="container pb-4">
            <div class="row">
                <div class="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto">
                    <div class="card mt-3 p-2">
                        <div class="card-body">
                            <div class="mt-1">
                                <p class="text-center c-primary small text-uppercase mt-1">Etapa @{{this.currentStage}} de @{{this.totalStages}}</p>
                                <div class="progress" style="height: 1px;">
                                    <div v-bind:style="{width: this.progressPercentage }" class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            
                            <div v-show="currentStage === 1" class="mt-5">
                                <p class="small text-uppercase font-weight-bold">Como deseas realizar tu registro:</p>
                                <div class="d-none d-sm-block">
                                    <div class="row mt-4">
                                        <div class="col-6 text-center">
                                            <div class="btn-group-toggle mx-auto" data-toggle="buttons" @click="radioListener('announcer')" >
                                                <label class="btn btn-outline-primary p-4 mh-203" 
                                                :class="[selected === 'announcer' ? 'active' : '']"
                                                id="announcer">
                                                    <i class="fad fa-person-sign d-block fa-3x d-block mx-auto mb-4"></i>
                                                    <input type="radio"  name="selected" id="option2" autocomplete="off"  v-model="selected" :checked="selected === 'announcer'" value="announcer" > Anunciante
                                                    <p class="small mb-0">Registrate para comenzar a implementar tus ATNDBOX.</p>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-6 text-center">
                                            <div class="btn-group-toggle mx-auto" @click="radioListener('company')" data-toggle="buttons">
                                                <label :class="selected === 'company' ? 'btn btn-outline-primary p-4 active mh-203' : 'btn btn-outline-primary p-4 mh-203'" id="offerer">
                                                    <i class="fad fa-user-tie d-block fa-3x d-block mx-auto mb-4"></i>
                                                    <input type="radio" name="selected" id="option1" autocomplete="off" v-model="selected" :checked="selected === 'company'" value="company"> Oferente
                                                    <p class="small mb-0">Registrate para comenzar a recibir solicitudes de atención.</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <p class="small text-uppercase font-weight-bold mt-4">Como anunciante podrás:</p>
                                                <div class="d-flex align-items-stretch bd-highlight">
                                                    <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight">Crear y editar tus ATNDBOX.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <p class="small text-uppercase font-weight-bold mt-4">Como oferente podrás:</p>
                                                <div class="d-flex align-items-stretch bd-highlight">
                                                    <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Recibir información de clientes que desean ser atendidos.</p>
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Comenzar a ganar dinero por lead generado.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Registrar y administrar a tus vendedores.</p>
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener datos relevantes para mejorar la fidelización a tu sitio web.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                    <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener reportes para mejorar la gestión de tu empresa.</p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="d-block d-sm-none">
                                    <div class="row mt-4">
                                        <div class="col-12 text-center">
                                            <p class="small text-uppercase font-weight-bold mt-4">Como anunciante podrás:</p>
                                            <div class="d-flex align-items-stretch bd-highlight">
                                                <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                <p class="small text-muted text-left w-100 bd-highlight">Crear y editar tus ATNDBOX.</p>
                                            </div>
                                            <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                <p class="small text-muted text-left w-100 bd-highlight mb-0">Comenzar a ganar dinero por lead generado.</p>
                                            </div>
                                            <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener reportes para mejorar la gestión de tu empresa.</p>
                                            </div>
                                        </div>

                                        <div class="col-12 text-center">
                                        <p class="small text-uppercase font-weight-bold mt-4">Como oferente podrás:</p>
                                            <div class="d-flex align-items-stretch bd-highlight">
                                                <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                <p class="small text-muted text-left w-100 bd-highlight mb-0">Recibir información de clientes que desean ser atendidos.</p>
                                            </div>
                                            <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                <p class="small text-muted text-left w-100 bd-highlight mb-0">Registrar y administrar a tus vendedores.</p>
                                            </div>
                                            <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                <i class="far fa-check-circle fa-lg c-success flex-shrink-1 bd-highlight mr-3"></i>
                                                <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener reportes para mejorar la gestión de tu empresa.</p>
                                            </div>
                                        </div>
                                    <div class="col-12 text-center mt-4">
                                            <div class="btn-group-toggle mx-auto" data-toggle="buttons" @click="radioListener('announcer')" >
                                                <label class="btn btn-outline-primary p-4" 
                                                :class="[selected === 'announcer' ? 'active' : '']"
                                                id="announcer">
                                                    <i class="fad fa-person-sign d-block fa-2x d-block mx-auto mb-4"></i>
                                                    <input type="radio"  name="selected" id="option2" autocomplete="off"  v-model="selected" :checked="selected === 'announcer'" value="announcer" > Anunciante
                                                    <p class="small mb-0">Registrate para comenzar a implementar tus ATNDBOX.</p>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-12 text-center mt-4">
                                            <div class="btn-group-toggle mx-auto" @click="radioListener('company')" data-toggle="buttons">
                                                <label :class="selected === 'company' ? 'btn btn-outline-primary p-4 active' : 'btn btn-outline-primary p-4'" id="offerer">
                                                    <i class="fad fa-user-tie d-block fa-2x d-block mx-auto mb-4"></i>
                                                    <input type="radio" name="selected" id="option1" autocomplete="off" v-model="selected" :checked="selected === 'company'" value="company"> Oferente
                                                    <p class="small mb-0">Registrate para comenzar a recibir solicitudes de atención.</p>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            {{-- End stage one --}}

                            <div v-show="currentStage === 2" class="card-body mt-2">
                                <div class="row">
                                    <div class="col-12 col-lg-12">
                                        <div class="small text-uppercase font-weight-bold mb-3">datos usuario</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Nombre</label>
                                            <input maxlength="125" v-model="forenames" type="text" class="form-control" id="store_forenames" required name="store_forenames" placeholder="Ingresar Nombre(s)">
                                        </div>
                                    </div>
                                    
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Apellidos</label>
                                            <input maxlength="125" v-model="surnames" type="text" class="form-control" id="store_surnames" required name="store_surnames" placeholder="Ingresar Apellido(s)">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">rut</label>
                                            <input  @input="validEmail" v-on:keyup.delete="validRut()" v-rut:live type="text" v-model="rut" maxlength="12" :class="msgRut.length ? 'form-control is-invalid' : (rutIsValid? 'form-control is-valid' : 'form-control')" required id="store_rut" name="store_rut" placeholder="Ingresar Rut">
                                            <small v-show="!rutIsValid" class="invalid-feedback">@{{msgRut}}</small>
                                        </div>
                                    </div>
                                    
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Correo Electrónico</label>
                                            <input maxlength="35" @keypress="validEmail(email)" @keyup.delete="validEmail(email)" v-model="email" type="text" :class="msg.length ? 'form-control is-invalid' : (emailIsValid? 'form-control is-valid' : 'form-control')" required id="store_email" name="store_email" value="{{ old('store_email') }}" placeholder="Ingresar correo">
                                            <small v-show="!emailIsValid" class="invalid-feedback">@{{msg}}</small>
                                        </div>
                                    </div> 
                                            
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Teléfono</label>
                                            <input type="tel" id="store_phone" name="store_phone" required class="form-control "  maxlength="9" @change="phoneListener()">
                                            <input v-show="false" type="text" v-model="phone" id="hidden_phone" name="hidden_phone" maxlength="12">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Contraseña</label>
                                            <input maxlength="20" v-model="password" :type="passwordFieldType1" class="form-control" required id="store_password" name="store_password" placeholder="Ingresar contraseña">
                                            <a class="btn btn-show-password-reg text-secondary" @click.prevent="switchVisibility1()"> @{{ isVisible1? 'Mostrar' : 'Ocultar' }}</a>
                                        </div>
                                    </div>  

                                    <div class="col-12">
                                        <div class="form-group mb-0">
                                            <label class="small text-muted text-uppercase">Confirmar contraseña</label>
                                            <input maxlength="20" v-model="password_confirm" :type="passwordFieldType2" class="form-control" required id="store_password_confirm" name="password_confirmation" placeholder="Confirmar contraseña">
                                            <a class="btn btn-show-password-reg text-secondary" @click.prevent="switchVisibility2()"> @{{ isVisible2? 'Mostrar' : 'Ocultar' }}</a>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            {{-- End stage two --}}

                            <div v-show="currentStage === 3" class="card-body mt-2">

                                <div class="row">

                                    <div class="col-12">
                                        <div class="custom-control custom-switch col-12 mb-4 c-primary">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch1" v-model="retrievePersonalData"  @change="handlePersonalDataChange">
                                            <label class="custom-control-label" for="customSwitch1">Recuperar datos personales</label>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-12">
                                        <div class="small text-uppercase font-weight-bold mb-3">datos empresa</div>
                                        
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Razón Social</label>
                                            <input maxlength="30" type="text" class="form-control" v-model="companyName" required id="store_name" value="{{ old('store_name') }}" name="store_name" placeholder="Ingresar Razón social">
                                        </div>
                                    </div>
                                    
                                    <div class="col-12 col-lg-12">
                                        
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Rut Empresa</label>
                                            <input type="text" @input="validRutCompany" v-on:keyup.delete="validRutCompany()" :class="msgRutCompany.length ? 'form-control is-invalid' : (companyRutIsValid? 'form-control is-valid' : 'form-control')" v-rut:live maxlength="12" v-model="companyRut" required id="store_rut_company" name="store_rut_company" placeholder="Ingresar Rut">
                                        </div>
                                    </div>

                                    <div v-if="selected === 'company'" class="col-12 col-lg-12">
                                        
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Nombre Fantasía</label>
                                            <input maxlength="30" type="text" v-model="fantasyName" class="form-control" required id="store_fantasy_name" name="store_fantasy_name" placeholder="Ingresar Nombre de fantasía">
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-12 mb-3">   
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Dirección</label>
                                            <input  maxlength="30" type="text" v-model="address" class="form-control" required id="store_address" name="store_address" value="{{ old('store_address') }}" placeholder="Ingresar Dirección">
                                        </div>
                                    </div>
                                    

                                    <div class="col-12 custom-controls-stacked d-block my-3" v-if="selected === 'announcer'">
                                        <label class="custom-control fill-checkbox">
                                            <input type="checkbox" class="fill-control-input"  required name="termsAndCond1" id="termAndCond1" @change="checkTerms()">
                                            <span class="fill-control-indicator"></span>
                                            <span class="fill-control-description small text-muted" for="termAndCond1">Al registrarme, declaro que soy mayor de edad y acepto los <a href="/termsAndConditions">Términos y condiciones y las Políticas de privacidad de primerocotiza.</a></span>
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div v-show="currentStage === 4" class="card-body mt-2">

                                <div class="row">
                                    <div v-if="selected === 'company'" class="col-12">
                                        <label class="small text-muted text-uppercase">Rubro</label>
                                        <select v-model="selectedArea" class="custom-select" id="store_area" name="store_area" value="{{ old('store_area') }}" @change="fillServices()" :required="selected === 'company'">
                                            <option  value="">Seleccione uno o mas rubros</option>
                                            @foreach($areas as $area)
                                            <option  value="{{$area->id}}">{{$area->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    

                                    <div class="col-12 mb-3 mt-3" v-if="selected === 'company'">
                                        <label class="small text-muted text-uppercase">servicio</label>
                                        <multiselect v-model="selectedServices" :options="options" :multiple="true"
                                        track-by="id" :close-on-select="false" label="name"></multiselect>
                                        <label class="small text-muted text-lowercase">*elimine los servicios que no desee activar</label>
                                    </div>

                                </div>
                            </div>





















                            {{-- End stage three --}}

                            <div v-show="this.selected === 'company' && currentStage === 5">
                                <div class="col-12 col-lg-12 mb-3" v-if="selected === 'company'">
                                    <p class="small text-muted text-uppercase">Serás vendedor en tu empresa?</p>

                                    <div class="form-check">
                                        <input v-model="isAttender" class="form-check-input" type="checkbox" id="ckx_attender" name="ckx_attender">
                                        <label class="form-check-label" for="ckx_attender">
                                            Si
                                        </label>
                                    </div>
                                    
                                    <div class="form-check mt-4">
                                        <input class="form-check-input" type="checkbox" class="form-control" required name="termsAndCond" id="termAndCond" @change="checkTerms()" >
                                        <label class="form-check-label small text-muted ml-2" for="termAndCond">
                                        Al registrarme, declaro que soy mayor de edad y acepto los <a href="/termsAndConditions">Términos y condiciones y las Políticas de privacidad de primerocotiza.</a> 
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {{-- End stage four --}}

                            <div class="col-12 card-body" id="buttons">
                                <button v-if="currentStage > 1" @click="prevStage()" type="button" class="btn btn-outline-primary">Volver</button>
                                <button v-if="currentStage !== totalStages" @click="nextStage()" type="button" class="btn btn-outline-success float-right" id="continueButton" :disabled="isDisabled">Continuar</button>
                                <button  v-if="currentStage === totalStages && isLoading=== false" type="button" @click="submitForm()" class="btn btn-success float-right" id="signinButton" :disabled="submitIsDisabled" >Registrar</button>
                                <button  v-if="isLoading=== true"  class="btn btn-success float-right" type="button" id="loadingSpinner" disabled="disabled">
                                     <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    <span class="sr-only"></span>Registrando ...
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</section>
</div>
</register-view>
@endsection

@section('template_scripts')
<script>
    $(document).ready(function() {
        let area_selected = '{{ old('store_area') }}';
        let type_selected = '{{old('store_type')}}';


        if (area_selected !== '') {
            $('#store_area').val(area_selected);
        }

        if (type_selected !== '') {
            $('#store_type').val(type_selected);
        }

    });
</script>
<script src="{{ asset('js/phoneinput.js') }}"></script>
<script src="{{ asset('js/app.signin.js') }}"></script>
@endsection