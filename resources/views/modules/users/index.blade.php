@extends('layouts.app')
@section('navbar')
@include('layouts.navbar')
@endsection

@section('content')

<user-view inline-template
			entities_list = "{{json_encode($entities->get())}}"
			branches_list =  "{{ ($branches == null ) ? null : json_encode($branches->get())}}"
			users_list = "{{json_encode($users->get())}}"
			role_list = "{{json_encode($role_list)}}"
>
	<div class="container">
		
		<div class="row">
			<div class="col-12 mt-2">
				{{-- @include("layouts.message") --}}
			</div>
		</div> 
		<div class="row">
			<div class="col-12 mt-2 small">
			</div>
		</div>
		<div class="row">
			<div class="col-12 d-flex align-items-center justify-content-between">
				<h5 class="mb-0 d-inline-block">Usuarios</h5>
				<button class="btn btn-md btn-success d-inline-block" data-toggle="modal" data-target="#createModal">
					<i class="far fa-layer-plus mr-1"></i>Agregar Usuario
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-6 col-lg-3 mt-3" id="search">
				<div class="form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						  <button class="input-group-text icon" id="searchButton_all">
							<div class="fas fa-search"></div>
						</button>
					  </div>
					  <input @input="filter(-1,-1)" class="form-control" placeholder="Buscar" maxlength="50" v-model="barFilter" id="search_filter_all" />
					</div>
				</div>
				

			</div>
		</div>
		@if(Auth::user()->hasAnyRole(['Superadmin']))
			<div class="col-12 mt-2">
				<div class="btn-group">
					<button @click="filter(0,0)" type="button" class="btn btn-primary btn-sm mr-1" >Todas las compañias</button>
				</div>
				<div v-for=" entity in entities" class="btn-group">
					<button @click="filter(entity.id,0)" type="button" class="btn btn-primary btn-sm mr-1" >@{{entity.name}}</button>
					<button  type="button" class="btn btn-primary btn-sm mr-1 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
					<div class="dropdown-menu">
						<a v-for="branch in entity.branches" @click="filter(entity.id,branch.id)" class="dropdown-item" aria-haspopup="true" aria-expanded="false" selected >@{{branch.name}}</a>
					</div>
				</div>  
			</div> 		  
		@endif
		@if(Auth::user()->hasAnyRole(['Owner']))
		<div class="col-12 mt-2">
				<div class="btn-group">
					<button @click="filter(0,0)" type="button" class="btn btn-primary btn-sm mr-1" >Todas las sucursales</button>
				</div>
				<div v-for="branch in branches" class="btn-group">
					<button @click="filter(entities[0].id,branch.id)" type="button" class="btn btn-primary btn-sm mr-1" >@{{branch.name}}</button>
				</div>
			</div>	  
		@endif
		<div class="row">
			<div class="col-12">
				<div class="card mt-3 table-responsive">
					<table class="table table-hover table-borderless nm">
						<thead>
							<tr>
								<th>{!! 'Nombres' !!}</th>
								<th>{!! 'Apellidos' !!}</th>
								<th>{!! 'Rut' !!}</th>
								<th>{!! 'Telefono' !!}</th>
								<th>{!! 'Email' !!}</th>
								<th>{!! 'Cargo' !!}</th>
								<th class="no-search no-sort">Acciones</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="user in users">
								<td >@{{user.forenames}}</td>
								<td >@{{user.surnames}}</td>
								<td >@{{user.rut}}</td>
								<td >@{{user.phone}}</td>
								<td >@{{user.email}}</td>
								<td v-show="user.role_id == 2" >Dueño</td>
								<td v-show="user.role_id == 3">Administrador</td>
								<td v-show="user.role_id == 4">Camarero</td>
								<td v-show="user.role_id == 5">Cajero</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<form id="form_create_user" action="" method="post">
			<div class="modal fade" id="createModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header d-flex align-items-center">
							<i class="far fa-user-plus fa-lg c-primary mr-2"></i>
							{{csrf_field()}}
							<h5 class="modal-title" id="exampleModalLabel">Crear usuario</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
		
									@if(Auth::user()->hasAnyRole(['Superadmin']))
										<div class="form-group mb-4">
											<input
											type="hidden"
											name="nid_type"
											v-model="selectedEntity">
											<label class="small text-muted text-uppercase">Entidad</label>
											<v-select 
											@input="filterByEntity()"
														v-model="selectedEntity"
														autocomplete="off"
														:options="entities"
														:reduce="entity => entity.id"
														:clearable="false"
														label="name"
														placeholder="Seleccione Entidad"
														select-on-tab>
												<slot name="search" slot-scope="slotProps">
												<input
													
													class="vs__search" 
													:required="!selectedEntity"

												/>
												
												</slot>
											</v-select>
										</div>
									@endif
									@if(Auth::user()->hasAnyRole(['Superadmin','Owner']))
										<div class="form-group mb-4">
											<input
												type="hidden"
												name="nid_type"
												v-model="selectedBranch">
											<label class="small text-muted text-uppercase">Sucursal</label>
											<v-select 
													
													v-model="selectedBranch"
													autocomplete="off"
													:disabled = "!selectedEntity"
													:options="filterBranches"
													:reduce="branch => branch.id"
													:clearable="false"
													label="name"
													placeholder="Seleccione Sucursal"
													select-on-tab>
												<slot name="search" slot-scope="slotProps">
													<input
														
														class="vs__search" 
														:required="!selectedBranch"
		
													/>
												
												</slot>
											</v-select>
										</div>
									@endif
		
									<div class="form-group mb-2">
										<label  class="small text-muted text-uppercase"for="store_forenames">Nombre </label>
										<input type="text" class="form-control" required id="store_forenames" name="store_forenames" placeholder="Ingresar Nombre(s)">
									</div>
									<div class="form-group mb-2">
										<label class="small text-muted text-uppercase" for="store_surnames">Apellidos</label>
										<input type="text" class="form-control" required id="store_surnames" name="store_surnames" placeholder="Ingresar Apellidos">
									</div>
		
									<div class="form-group mb-4">
										<label class="small text-muted text-uppercase" for="store_email">Email</label>
										<input type="text" class="form-control" required id="store_email" name="store_email" placeholder="Ingresar Email">
									</div>
									<div class="form-group mb-4">
										<input
											type="hidden"
											name="nid_type"
											v-model="selectedRole">
										<label class="small text-muted text-uppercase">Rol</label>
										<v-select 
												v-model="selectedRole"
												autocomplete="off"
												:options="roles"
												:reduce="role => role.id"
												:clearable="false"
												label="name"
												placeholder="Seleccione Entidad"
												select-on-tab>
											<slot name="search" slot-scope="slotProps">
												<input
													
													class="vs__search" 
													:required="!selectedRole"
	
												/>
											
											</slot>
										</v-select>
									</div>

								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
							<div id="submitCreateButton">
								<button type="submit" class="btn btn-primary">Guardar</button>
							</div>
							<button class="btn btn-primary" type="button" id="loadingSpinner" style="display:none;">
								<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
								<span class="sr-only"></span>Enviando Invitación ...
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		
	</div>
</user-view>

@endsection
