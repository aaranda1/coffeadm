@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-10 col-md-6 mx-auto">
            <div class="card mt-5 text-center">
            <div class="card-header bg-light">
                <img src="https://i.ibb.co/nmGc3zV/primerocotiza.png" alt="primerocotiza" border="0" style="width: 170px;margin: 0 auto 1px;display: block;" />
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-horizontal text-center">
                        <div class="fa-4x">
                            <span class="fa-layers fa-fw faa-horizontal animated">
                                <i class="far fa-envelope c-primary"></i>
                                <span class="fa-layers-counter text-light">1</span>
                            </span>
                        </div> 
                       
                    <h5 class="card-title mb-0 d-inline-block">Gracias por registrarte en PrimeroCotiza</h5>
                        <p>Te hemos enviado un correo para que finalices tu registro.</p>
                        
                     
                        <a href="/login">Volver</a>        
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection


