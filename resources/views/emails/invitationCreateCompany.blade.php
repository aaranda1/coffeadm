<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <title></title>
  
  <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,600&display=swap" rel="stylesheet">
</head>

<body>
  <table style="max-width: 500px; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: 'Work Sans', sans-serif !important; text-align: center;">
    <tr>
      <td style="background-color: #ecf0f1; text-align: left; padding: 0">
      </td>
    </tr>

    <tr>
      <td>
        <div>
         
          <div class="d-flex justify-content-center">
            <div class="col-6 text-center mt-3 ">
                <div class="btn-group-toggle mx-auto" >
                    <i class="far fa-mug-hot d-block fa-3x d-block mx-auto mb-4 " width="50%" ></i>
                    <img class="mb-4 " src="https://i.ibb.co/P6hDsN8/output-onlinepngtools.png" alt="" width="50%" />
                    
                </div>
            </div>
        </div>
          
          <h2 style="color: black; margin: 4px 0">Hola <strong>{{$name}}!</strong> </h2>
          <p style="font-size: 15px">
          Para finalizar el registro de tu negocio <strong>{{$company_name}}</strong> presiona el siguiente enlace:
          </p>
          <br/>
          <div style="margin-top:40px; text-align: center">
			<a style="background: black;padding: .8rem 2.5rem;border-radius: .8rem;color: #ffffff;text-transform: uppercase;text-decoration: none;font-weight: bold;" href="{{URL::to('/accountConfirmation/'.$token)}}">Confirmar registro</a>
		  </div>
            <br/>
            <br/>
            <br/>
              <p>Si tienes alguna duda, contáctanos a <strong>contacto@coffeADM.cl</strong></p>
              <p style="color: #b3b3b3;font-size: 12px;text-align: center;margin: 40px 0 0;text-transform: uppercase;border-bottom: 1px solid #eaeaea;padding: 10px;">coffeADM</p>

        </div>
      </td>
    </tr>
  </table>
  <script src="https://kit.fontawesome.com/6e99aa4256.js"></script>
</body>
</html>