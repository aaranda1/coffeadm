@extends('layouts.app')

@section('content')
<login-view inline-template>
<section class="bg-light" style="background-image: url('https://perfectdailygrind.com/wp-content/uploads/2019/02/bussy-coffee-shop.jpg');background-size: 100% 100%;">
    <div v-cloak class="container-fluid">
        <div class="row" >
            <div class="col-12 col-md-6 col-lg-5 col-xl-4 " style="background-color: rgba(255, 255, 255, 0.9);">
                @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
                @endif

                <div class="d-flex align-items-center vh-100" id="login">
                    <form id="loginForm" class="form-signin needs-validation col" method="POST" action="{{ route('login')}}">
                        {{ csrf_field() }}


                        <div class="text-center py-5 mb-4">
                            <i class="far fa-mug-hot d-block fa-5x d-block py-1 mx-auto mb-4 " width="50%" ></i>
                            <img class="mb-4 " src="https://i.ibb.co/P6hDsN8/output-onlinepngtools.png" alt="" width="50%" />
                        </div>
                        

                        <div class="container">  
                            <div class="form-group">
                                <input maxlength="35" id="email"  type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo Electrónico" required autofocus onchange="lowcase(this.value)">    
                                <div class="invalid-feedback">
                                    Ingresar correo válido
                                </div>
                            </div>
                            <div class="form-group">
                                <input maxlength="30" v-model="password" id="password" :type="passwordFieldType" class="form-control" name="password" type="password" placeholder="Contraseña" value="" required>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    @include("layouts.message")
                                </div>
                            </div>
    
    
    
                            
                            <div class="custom-control custom-switch w-100">
    
                                <div class="d-flex justify-content-between align-items-center">
                                        <input type="checkbox" class="custom-control-input" id="remember" name="remember" novalidate>
                                        <label class="custom-control-label" for="remember">Recuérdame</label>
    
                                    <a class="btn-link small" href="{{ route('password.request') }}">Olvidaste tu contraseña?</a>
                                </div>
                                
                            </div>
                            <button style="background: black;border-radius: .8rem;color: #ffffff;text-decoration: none;" class="btn btn-primary btn-block mt-5" type="submit">Ingresar</button>
    
                            <a href="/register" class="btn btn-link btn-block mt-4">Registrarse</a>                   
                        </form>
                        <form id="resendForm" method="POST" action="">
                            {{csrf_field()}}
                            
                            <input maxlength="35" id="emailResend" name="emailResend" type="hidden" value="{{ old('email') }}">
                        
                        </form>
                        </div>
                        
                       {{--  <input maxlength="35" id="email" type="hidden"  name="email" > --}}
  
                </div>
            </div>

            

            <div class="col-12 col-md-6 col-lg-7 col-xl-8 login_bg np d-none d-lg-block vh-100">
                
                
            </div>
        </div>
    </div>
</section>
</login-view>
@endsection



