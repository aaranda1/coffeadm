@extends('layouts.app')

@section('content')
<register-view inline-template>


<section>
    <form id="signin_form" method="POST" >
        {{ csrf_field() }}
        <div class="container">
            <div id="alert_msg" class="mt-3">
                @include("layouts.message")
            </div>
        </div>
        <div class="container pb-4">
            <div class="row">
                <div class="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto">
                    <div class="card mt-3 p-2">
                        <div class="card-body">
                            <div class="mt-1">
                                <p class="text-center c-primary small text-uppercase mt-1">Etapa @{{this.currentStage}} de @{{this.totalStages}}</p>
                                <div class="progress" style="height: 1px;">
                                    <div v-bind:style="{width: progressPercentage }" class="progress-bar " role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="col-6 text-center mt-3 ">
                                    <div class="btn-group-toggle mx-auto" >
                                        <i class="far fa-mug-hot d-block fa-3x d-block mx-auto mb-4 " width="50%" ></i>
                                        <img class="mb-4 " src="https://i.ibb.co/P6hDsN8/output-onlinepngtools.png" alt="" width="50%" />
                                        
                                    </div>
                                </div>
                            </div>
                            <div v-show="currentStage === 1" class="mt-5">
                                <div class="d-none d-sm-block">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <p class="small text-uppercase font-weight-bold mt-4">Como Owner podras:</p>
                                                <div class="d-flex align-items-stretch bd-highlight">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight">Crear o eliminar tus sucursales.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <p class="small text-uppercase font-weight-bold mt-4">Tus trabajadores podran:</p>
                                                <div class="d-flex align-items-stretch bd-highlight">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Tomar pedidos de forma mas optima atravez de una interfaz virtual.</p>
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener reportes para mejorar la gestión de tu negocio.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Calcular, dividir y/o cobrar cuentas por mesa..</p>
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Asignar un administrador a tus sucursales.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Ver sus mesas atendidas.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Ver un historial de ventas por sucursal.</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0"></p>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="d-block d-sm-none">
                                        <div class="row mt-4">
                                            <div class="col-12 text-center">
                                                <p class="small text-uppercase font-weight-bold mt-4">Como Owner podras:</p>
                                                <div class="d-flex align-items-stretch bd-highlight">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight">Crear y editar tus ATNDBOX.</p>
                                                </div>
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Comenzar a ganar dinero por lead generado.</p>
                                                </div>
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener reportes para mejorar la gestión de tu empresa.</p>
                                                </div>
                                            </div>
    
                                            <div class="col-12 text-center">
                                            <p class="small text-uppercase font-weight-bold mt-4">Tus trabajadores podran:</p>
                                                <div class="d-flex align-items-stretch bd-highlight">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Recibir información de clientes que desean ser atendidos.</p>
                                                </div>
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Registrar y administrar a tus vendedores.</p>
                                                </div>
                                                <div class="d-flex align-items-stretch bd-highlight mt-3">
                                                    <i class="far fa-check-circle fa-lg text-success flex-shrink-1 bd-highlight mr-3"></i>
                                                    <p class="small text-muted text-left w-100 bd-highlight mb-0">Obtener reportes para mejorar la gestión de tu empresa.</p>
                                                </div>
                                            </div>
    
                                        </div>
                                        
                                    </div>
                                </div>
 
                            </div>
                            {{-- End stage one --}}

                            <div v-show="currentStage === 2" class="card-body mt-2">
                                <div class="row">
                                    <div class="col-12 col-lg-12">
                                        <div class="small text-uppercase font-weight-bold mb-3">Tus datos</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 ">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Nombre</label>
                                            <input maxlength="125" v-model="forenames" type="text" class="form-control" id="user_forenames" name="user_forenames" placeholder="Ingresar Nombre(s)">
                                            <small class="invalid-feedback">Debe ingresar un nombre</small>
                                        </div>
                                    </div>
                                    
                                    <div class="col-12 ">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Apellidos</label>
                                            <input maxlength="125" v-model="surnames" type="text" class="form-control" id="user_surnames" name="user_surnames" placeholder="Ingresar Apellido(s)">
                                            <small class="invalid-feedback">Debe ingresar un apellido</small>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">rut</label>
                                            <input      v-rut:live type="text" v-model="rut" maxlength="12" :class="(rutIsValid ? 'form-control' : 'form-control is-invalid')" required id="store_rut" name="store_rut" placeholder="Ingresar Rut">
                                            <small v-show="!rutIsValid" class="invalid-feedback">@{{msgRut}}</small>
                                        </div>
                                    </div>
                                    
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Correo Electrónico</label>
                                            <input maxlength="35"  v-model="email" type="text" :class="(emailIsValid ? 'form-control' : 'form-control is-invalid')" required id="store_email" name="store_email" value="{{ old('store_email') }}" placeholder="Ingresar correo">
                                            <small v-show="!emailIsValid" class="invalid-feedback">@{{msg}}</small>
                                        </div>
                                    </div> 

                                    <div class="col-12">
                                        <label class="small text-muted text-uppercase" for="store_ammount">Teléfono</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend">+569</span>
                                            </div>
                                            <input v-model="phone" type="tel" id="user_phone" name="user_phone"  class="form-control "  maxlength="9" >
                                            <small class="invalid-feedback">Debe ingresar un numero de telefono valido</small>
                                        </div>
                                    </div>


                                    <div class="col-12 my-4">
                                        <div class="form-group">
                                            <label class="small text-muted text-uppercase">Contraseña</label>
                                            <input maxlength="20" v-model="password" :type="passwordFieldType1" class="form-control"  id="user_password" name="user_password" placeholder="Ingresar contraseña">
                                            <small class="invalid-feedback">La contraseña debe contener al menos 6 caracteres</small>
                                        </div>
                                    </div>  

                                    <div class="col-12">
                                        <div class="form-group mb-0">
                                            <label class="small text-muted text-uppercase">Confirmar contraseña</label>
                                            <input maxlength="20" v-model="password_confirm" :type="passwordFieldType2" class="form-control"  id="user_password_confirm" name="user_password_confirm" placeholder="Confirmar contraseña">
                                            <small class="invalid-feedback">No coincide</small>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            {{-- End stage two --}}

                            

                                <div v-show="currentStage === 3" class="card-body mt-2">

                                    <div class="row">
    
    
                                        <div class="col-12">
                                            <div class="small text-uppercase font-weight-bold mb-3">Datos de tu negocio</div>
                                            
                                            <div class="form-group">
                                                <label class="small text-muted text-uppercase">Nombre</label>
                                                <input maxlength="30" type="text" class="form-control" v-model="companyName" id="entity_name" name="entity_name" placeholder="Ingresar Razón social">
                                                <small class="invalid-feedback">Debe ingresar un nombre</small>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12">
                                            
                                            <div class="form-group">
                                                <label class="small text-muted text-uppercase">Rut Empresa</label>
                                                <input type="text"  :class="(companyRutIsValid? 'form-control' : 'form-control is-invalid')" v-rut:live maxlength="12" v-model="companyRut" required id="rut_company" name="rut_company" placeholder="Ingresar Rut">
                                                <small v-show="!rutIsValid" class="invalid-feedback">@{{msgRutCompany}}</small>
                                            </div>
                                        </div>                                                                                                     

    
                                        <div class="col-12 ">   
                                            <div class="form-group">
                                                <label class="small text-muted text-uppercase">Dirección</label>
                                                <input  maxlength="30" type="text" v-model="address" class="form-control" required id="address_company" name="address_company" value="{{ old('store_address') }}" placeholder="Ingresar Dirección">
                                                <small class="invalid-feedback">Debe ingresar una direccion</small>
                                            </div>
                                        </div>   
                                        
                                        <div class="form-group col-12">
                                            <label class="small text-muted text-uppercase">Region</label>
                                            
                                            <v-select  @input="getCommunes"
                                                      v-model="selectedRegion"
                                                      autocomplete="off"
                                                      
                                                      :options="regions"
                                                      :reduce="region => region.id"
                                                      :clearable="false"
                                                      label="name"
                                                      placeholder="Seleccione region"
                                                      select-on-tab>
                                              <slot name="search" slot-scope="slotProps">
                                                <input
                                                  class="vs__search"
                                                  :required="!selectedRegion"

                                                />
                                              </slot>
                                            </v-select>
                                            <div class="invalid-feedback d-block">
                                              @error('nid_type')
                                              {{$message}}
                                              @enderror
                                            </div>
                                          </div>
    
                                          <div class="form-group col-12">
                                            <label class="small text-muted text-uppercase">Comuna</label>
                                            <v-select 
                                                    @input = "getCities"
                                                    v-model="selectedCommune"
                                                      autocomplete="off"
                                                      :options="communes"
                                                      :disabled = "!selectedRegion"
                                                      :reduce="commune => commune.id"
                                                      :clearable="false"
                                                      label="name"
                                                      placeholder="Seleccione comuna"
                                                      select-on-tab>
                                              <slot name="search" slot-scope="slotProps">
                                                <input
                                                  class="vs__search"
                                                  :required="!selectedCommune"

                                                />
                                              </slot>
                                            </v-select>
                                            <div class="invalid-feedback d-block">
                                              @error('nid_type')
                                              {{$message}}
                                              @enderror
                                            </div>                                          

                                        </div>
                                                                                    
                                        <div class="form-group col-12">
                                            <input
                                            type="hidden"
                                            name="nid_type"
                                            v-model="selectedCity">
                                            <label class="small text-muted text-uppercase">Ciudad</label>
                                            <v-select 
                                                        
                                                        v-model="selectedCity"
                                                        autocomplete="off"
                                                        :options="cities"
                                                        :disabled = "!selectedCommune"
                                                        :reduce="commune => commune.id"
                                                        :clearable="false"
                                                        label="name"
                                                        placeholder="Seleccione comuna"
                                                        select-on-tab>
                                                <slot name="search" slot-scope="slotProps">
                                                <input
                                                    
                                                    class="vs__search" 
                                                    :required="!selectedCity"

                                                />
                                                
                                                </slot>
                                            </v-select>

                                            </div>

                                    </div>
                                </div>
    
    

                            {{-- End stage three --}}

                            <div v-show="currentStage === 4">
                                <div class="container">
                                    <div class="card shadow-sm mb-4">
                                        <div class="card-body">
                                            
                                            <div class="contract-box mb-5">
                                                <p class="text-center">Términos y condiciones</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean scelerisque, urna vitae suscipit tristique, dui urna suscipit tortor, vitae dapibus lacus dolor in erat. Nunc convallis feugiat nunc, et dictum sem suscipit eget. In mauris ante, consequat id tincidunt non, pretium ut mi. Suspendisse et ipsum sit amet mi lobortis mattis. Cras imperdiet ante eu orci dignissim, vel interdum tortor auctor. Aenean non commodo diam, sit amet finibus nisl. Donec ut consequat turpis. Vivamus commodo diam tellus, eget sagittis elit malesuada id. Ut vel semper dui.</p>
                    
                                                <p>Nulla facilisi. Cras efficitur et lorem nec eleifend. Vestibulum suscipit eros id massa semper, ac fringilla quam condimentum. Etiam scelerisque enim ut leo dignissim pharetra. Ut tincidunt tincidunt ipsum. In eu tellus finibus, malesuada ligula eget, malesuada augue. Donec placerat arcu quis nisi iaculis molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    
                                                <p>Nulla vestibulum, quam sed porttitor iaculis, mauris quam blandit ipsum, a egestas massa nisi in urna. Etiam porttitor dolor at velit auctor, eget hendrerit augue dictum. Pellentesque eget massa porta, aliquet odio at, iaculis ex. Praesent ultrices euismod libero eget placerat. Cras sit amet pulvinar eros. Donec pellentesque risus non commodo varius. Praesent sit amet enim a turpis varius vulputate.</p>
                    
                                                <p>Maecenas vitae augue ut ex pharetra lacinia. Nullam at dapibus leo, non cursus metus. Curabitur tincidunt hendrerit eros facilisis vehicula. Nunc pellentesque felis id consequat iaculis. Duis eget suscipit justo. Aenean et diam non nunc maximus porttitor. Nulla facilisi. Suspendisse potenti. Sed molestie dictum mi fermentum dictum. Aenean dignissim lobortis iaculis. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 mb-3" >
                                    <div class="col-12 custom-controls-stacked d-block my-3" >
                                        <label class="custom-control fill-checkbox">
                                            <input type="checkbox" class="fill-control-input"  id="termAndCond1" v-model="terms">
                                            <span class="fill-control-indicator"></span>
                                            <span class="fill-control-description small text-muted" for="termAndCond1">Al registrarme, declaro que soy mayor de edad y acepto los <a>Términos y condiciones y las Políticas de privacidad de primerocotiza.</a></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {{-- End stage four --}}

                            <div class="col-12 card-body" id="buttons">
                                <button :disabled="isLoading"  @click="prevStage()"  type="button" class="btn btn-outline-primary">Volver</button>
                                
                                <button v-if="currentStage !== totalStages"  @click="nextStage()"  type="button" class="btn btn-outline-primary float-right" id="continueButton" >Continuar</button>
                                <button  v-if="currentStage === totalStages && isLoading=== false" type="button" @click="submit()"  {{-- @click="submitForm()" --}} class="btn btn-success float-right" id="signinButton" :disabled="!terms" >Registrar</button>
                                <button  v-if="isLoading=== true"  class="btn btn-success float-right" type="button" id="loadingSpinner" disabled="disabled">
                                     <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    <span class="sr-only"></span>Registrando ...
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
    <div class="modal fade" id="modalActivation" tabindex="-1" role="dialog" aria-labelledby="modalActivation" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times"></i>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-sm-8 mx-auto">
                            <i class="fa fa-envelope-o text-primary fa-4x mb-4"></i>
                            <h5 class="text-secondary">¡Ya casi!</h5>
                            <p class="mb-0">Te hemos enviado un mail para confirmar el registro de tu cuenta</p>
                        </div>
                    </div>
                </div>
                <div class="text-center pt-3 pb-5">
                    <button type="button" style="background: black;" class="btn btn-primary" data-dismiss="modal">Entendido</button>
                </div>
            </div>
        </div>
    </div>
</section>

</register-view>
@endsection

