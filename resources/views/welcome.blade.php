@extends('layouts.app')
@section('navbar')
@include('layouts.navbar')
@endsection
@section('content')
<welcome-view inline-template>
<div class="container">
    <div class="row">

{{--         <div class="col-10 col-md-7 mx-auto">
            <div class="card mt-5">
                <div class="card-header bg-light p-3 d-flex align-items-center justify-content-between">
                    <img src="images/coffeLogo.png" width="25%" class="float-left"/>
                <a href="/ownerReportView">
                    <button type="button" class="btn float-right">
                        <p class="text-uppercase small float-right mb-0">omitir</p>
                    </button>
                </a>
                </div> 
                <div class="card-body text-center p-5 mh-270">
                    <transition :name="action === 'next' ? 'slide-left' : 'slide-right'" mode="out-in">
                        <div key="1" id="ob-1" v-if="currentStage === 1">
                            <h6 class="text-uppercase  font-weight-bold mb-4">paso 1</h6>
                            <i class="fad fa-copy fa-5x c-primary pulse mb-4"></i>
                            <p class="small text-muted mb-0 text-uppercase">Selecciona, edita e inserta tu atnbox a la plataforma que desees...</p>
                        </div>
                        <div key="2" id="ob-2" v-if="currentStage === 2">
                            <h6 class="text-uppercase  font-weight-bold mb-4">paso 2</h6>
                            <i class="fad fa-hands-usd fa-5x c-primary pulse mb-4"></i>
                            <p class="small text-muted mb-0 text-uppercase">Gana dinero por cada lead que complete un atenbox que tengas activo!</p>
                        </div>
                        <div key="3" id="ob-3" v-if="currentStage === 3">
                            <h6 class="text-uppercase  font-weight-bold mb-4">paso 3</h6>
                            <i class="fad fa-chart-line fa-5x c-primary pulse mb-4"></i>
                            <p class="small text-muted mb-0 text-uppercase">Ve como aumenta el flujo de leads en tus sitios...</p>
                        </div>    
                        <div key="4" id="ob-4" v-if="currentStage === 4">
                            <h6 class="text-uppercase  font-weight-bold mb-4">paso 4</h6>
                            <i class="fad fa-handshake fa-5x c-primary pulse mb-4"></i>
                            <p class="small text-muted mb-0 text-uppercase">Se te pagará por lead generado y/o verificado.</p>
                            <p class="small text-muted mb-0 text-uppercase">La forma de pago será el acumulado de leads mensualmente...</p>
                        </div>
                    </transition>
                </div>

                <div class="card-footer bg-light d-flex align-items-center justify-content-between">
                        
                        <button :disabled="currentStage === 1" type="button" @click="prevStage()" class="btn">
                            <p class="text-uppercase small float-right mb-0">volver</p>
                        </button>
                    
                    <div class="">
                        <a @click="setStage(1)">
                            <div :class="currentStage === 1 ? 'dot active' : 'dot'"></div>
                        </a>
                        <a @click="setStage(2)">
                            <div :class="currentStage === 2 ? 'dot active' : 'dot'"></div>
                        </a>
                        <a @click="setStage(3)">
                            <div :class="currentStage === 3 ? 'dot active' : 'dot'"></div>
                        </a>
                        <a @click="setStage(4)">
                            <div :class="currentStage === 4 ? 'dot active' : 'dot'"></div>
                        </a>
                    </div>

                    
                        <button :disabled="currentStage === 4" type="button" @click="nextStage()" class="btn">
                            <p class="text-uppercase small float-right mb-0 c-primary">siguiente</p>
                        </button>
                    
                </div>
            </div>
        </div> --}}
    </div>
</div>
</welcome-view>
@endsection