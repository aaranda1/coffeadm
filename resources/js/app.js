/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import vSelect from 'vue-select'
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('welcome-view',require('./components/Welcome.vue'))
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('register-view',require('./components/Register.vue')); 
Vue.component('user-view',require('./components/Users.vue')); 
Vue.component('v-select', vSelect)
Vue.component('login-view',require('./components/login.vue'))
/* Vue.component('register-view', () =>
    import('./components/Register.vue')
); */
const app = new Vue({
    el: '#app',
});
